import {Children} from './children';

export type Dir = (...items: Children) => (path?: string) => void;
