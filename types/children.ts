import {File} from './file';

export type Children = (File | Children)[];
