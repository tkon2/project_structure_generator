import {Faker} from '@faker-js/faker';
import type {File} from '../types/file';
import {createFakerInstance} from "./fakerHelpers";

/** Use a faker instance to generate data */
export const gen = <T extends File|File[]>(f: (helpers: Faker) => T): T => {
  return f(createFakerInstance());
}

