import {Children} from "../types/children";
import {Dir} from "../types/dir";
import {listify} from "./helpers";

/** Represents a directory that can contain other items */
export const dir = (name: string): Dir => (...items: Children) => (path: string = '.') => {
  for (let f of listify(items)) {
    f(`${path}/${name}`);
  }
}

