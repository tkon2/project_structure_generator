import type {File} from '../types/file';
import type {Children} from '../types/children';

export const listify = (items: Children): File[] => {
  let out: File[] = [];
  for (let item of items) {
    if (Array.isArray(item)) {
      out = [...out, ...listify(item)];
    } else {
      out.push(item);
    }
  }

  return out;
}

