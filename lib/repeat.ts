import {Faker} from '@faker-js/faker';
import type {File} from '../types/file';
import {createFakerInstance} from './fakerHelpers';

/**
 * Repeat the contents a set number of times.
 * Provides a faker instance for each iteration
 */
export const repeat = (count: number) => (f: (helpers: Faker) => File|File[]): File[] => {
  const sub: File[] = [];
  for (let i = 0; i < count; i++) {
    const list = f(createFakerInstance());
    if (Array.isArray(list)) {
      for (let item of list) {
        sub.push(item);
      }
    } else {
      sub.push(list);
    }
  }
  return sub;
}
