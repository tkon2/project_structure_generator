import {Faker, Randomizer, en} from "@faker-js/faker";

class StableRandomizer implements Randomizer {
  currentSeed = Math.random();

  next() {
    return this.currentSeed;
  }

  seed(val: number|number[]) {
    this.currentSeed = (
      typeof val === 'number'
        ? val
        : val[0]
    ) ?? Math.random();
  }
}


export const createFakerInstance = () => {
  const f = new Faker({locale: [en], randomizer: new StableRandomizer()});
  return f;
}

