import {dir} from './lib/dir';
import {file} from './lib/file';
import {gen} from './lib/gen';
import {repeat} from './lib/repeat';

console.log('\n\n');
dir('proj')(
  file('yarn.lock'),
  file('package.json'),
  dir('src')(
    dir('types')(
      repeat(20)(f =>
         file(`${f.commerce.product()}.ts`)
      ),
    ),
    dir('components')(
      repeat(5)(f =>
        dir(`${f.commerce.product()}`)(
          file('index.tsx'),
          file('style.scss'),
          file('test.ts'),
        )
      ),
    ),
    dir('alternativeComponents')(
      repeat(3)(f => [
        file(`${f.commerce.product()}.tsx`),
        file(`${f.commerce.product()}.test.ts`),
        file(`${f.commerce.product()}.scss`),
      ])
    ),
    dir('convert')(
      repeat(5)(f => (
        gen(f2 => [
          file(`${f.commerce.product()}to${f2.commerce.product()}.ts`),
          file(`${f.commerce.product()}to${f2.commerce.product()}.test.ts`),
        ])
      ))
    )
  )
)()
